import React from 'react';
import { string, number } from 'prop-types';

function Decoration({ color, rotate, ...props }) {
  return (
    <svg {...props} viewBox="0 0 745 125" transform={`rotate(${rotate})`}>
      <path
        d="M372.5 87.333l41.667-41.666L372.5 4l-41.667 41.667L372.5 87.333zm0 0L330.833 129M372.5 87.333L414.167 129m-1.042-1.042l41.667-41.666 41.666 41.666m-247.916 0l41.666-41.666 41.667 41.666M495 127.333h250m-495 0H0"
        stroke={color}
        strokeWidth={5}
      />
    </svg>
  );
}

Decoration.defaultProps = {
  fill: 'none',
  color: '#f0efeb',
  rotate: 0
};

Decoration.propTypes = {
  fill: string,
  width: number,
  height: number,
  color: string,
  rotate: number
};

export default Decoration;
