import React from 'react';
import { render } from '@testing-library/react';
import Content from '../Content';

describe('<Content />', () => {
  it('Should Component Renders With Default Props', () => {
    const { queryByText } = render(<Content overlaid={false} />);

    expect(queryByText('Hello World')).toBeInTheDocument();
    expect(queryByText('Lorem Ipsum Dolor Sit Amet')).toBeInTheDocument();
  });

  it('Should Component Renders With Custom Props', () => {
    const { queryByText } = render(
      <Content title="Monument Valley" subTitle="Made With CRA Template" overlaid={true} />
    );

    expect(queryByText('Monument Valley')).toBeInTheDocument();
    expect(queryByText('Made With CRA Template')).toBeInTheDocument();
  });
});
